// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import firebase from 'firebase'

Vue.config.productionTip = false

let app;
var config = {
  apiKey: "AIzaSyB0IGK_iz07jx794ls66tjcsHMlVIRv01s",
  authDomain: "linkcollect-49648.firebaseapp.com",
  databaseURL: "https://linkcollect-49648.firebaseio.com",
  projectId: "linkcollect-49648",
  storageBucket: "linkcollect-49648.appspot.com",
  messagingSenderId: "154044802480"
};

firebase.initializeApp(config);
firebase.auth().onAuthStateChanged((user) =>{
  if(!app){
    /* eslint-disable no-new */
    app = new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>'
    })
  }
})


